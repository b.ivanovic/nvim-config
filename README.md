# Nvim config

Neovim config


## Installation notes for a fresh setup
* install lua
* run ```git clone --depth 1 https://github.com/wbthomason/packer.nvim\ ~/.local/share/nvim/site/pack/packer/start/packer.nvim```
* open ```init.lua``` and comment out the all the plugin config ```require```s
* run nvim and ```PackerInstall```
* uncomment the ```require```s in init.lua
