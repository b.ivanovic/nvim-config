-- custom script to build notes and store them in the Dropbox directory
vim.cmd 'autocmd BufWritePost *note-*.md silent !/home/shocky/bin/buildNote %:p'
