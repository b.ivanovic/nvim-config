return require('packer').startup(function(use) 
    -- packer
        use 'wbthomason/packer.nvim'

    -- prereqs
        use 'nvim-lua/plenary.nvim'

    -- themes
        use 'morhetz/gruvbox'
        use 'lifepillar/vim-gruvbox8'
        use 'marko-cerovac/material.nvim'
        use 'andersevenrud/nordic.nvim'
        use 'folke/tokyonight.nvim'
        use 'xiyaowong/nvim-transparent'
        use 'rebelot/kanagawa.nvim'

    -- file explorer
        use 'kyazdani42/nvim-tree.lua'

    -- git
        use 'lewis6991/gitsigns.nvim'

    -- completion
        use 'hrsh7th/nvim-cmp'
        use 'hrsh7th/cmp-buffer'
        use 'hrsh7th/cmp-path'
        use 'hrsh7th/cmp-cmdline'
        use 'hrsh7th/cmp-nvim-lsp'
        use 'saadparwaiz1/cmp_luasnip'
        use 'windwp/nvim-autopairs'

    -- lsp
        use 'neovim/nvim-lspconfig'
        use 'williamboman/nvim-lsp-installer'

    -- syntax
        use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}
        use 'p00f/nvim-ts-rainbow'

    -- markdown
        use {"ellisonleao/glow.nvim"}

    -- terminal
        use 'numToStr/FTerm.nvim'

    -- snippets

    -- fuzzy find
        use 'nvim-telescope/telescope.nvim'

    -- notes
    -- utility
        use 'matbme/JABS.nvim'
        use 'justinmk/vim-sneak'
        use 'folke/which-key.nvim'
        use 'rcarriga/nvim-notify'

    -- debugging
    -- tabline
    -- statusline
    -- languages
    -- motion
    -- editing support
        use 'tpope/vim-commentary'

    -- formatting
    -- icons
        use 'kyazdani42/nvim-web-devicons'
end)
