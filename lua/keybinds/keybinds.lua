local map = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }

map('', '<Space>', '<Nop>', opts)
vim.g.mapleader = ' '

map('n', '<Leader>w', ':write<CR>', opts)
map('n', '<Leader>h', ':nohlsearch<CR>', opts)
map('n', 'Q', ':wqa<CR>', opts)
map('n', 's', ':w<CR>', opts)
map('n', 'S', ':wa<CR>', opts)

-- switch between windows
map('n', '<C-h>', '<C-w>h', opts)
map('n', '<C-j>', '<C-w>j', opts)
map('n', '<C-k>', '<C-w>k', opts)
map('n', '<C-l>', '<C-w>l', opts)

-- buffers
map('n', '<S-l>', ':bnext<CR>', opts)
map('n', '<S-h>', ':bprevious<CR>', opts)
map('n', '<Leader>c', ':Bdelete<CR>', opts)

-- don't deselect on indent
map('v', '<', '<gv', opts)
map('v', '>', '>gv', opts)

-- open file in default application
map('n', '<Leader>x', ':!xdg-open %<CR><CR>', opts)

-- edit file under cursor (creates new file if missing)
map('', 'gf', ':tabedit <cfile><CR>', opts)

-- resize splits
map('n', '<C-Left>', ':vertical resize -5<CR>', opts)
map('n', '<C-Right>', ':vertical resize +5<CR>', opts)
map('n', '<C-Down>', ':resize +5<CR>', opts)
map('n', '<C-Up>', ':resize -5<CR>', opts)

-- move lines
map('n', '<A-j>', ':m +1<CR>==', opts)
map('n', '<A-k>', ':m -2<CR>==', opts)
map('v', '<A-j>', ":m '>+1<CR>==", opts)
map('v', '<A-k>', ":m '<-2<CR>==", opts)
map('x', '<A-j>', ":m '>+1<CR>gv-gv", opts)
map('x', '<A-k>', ":m '<-2<CR>gv=gv", opts)

-- don't put overwritten text into the register
map('v', 'p', '"_dP', opts)

------------------------------------------------------------------------
-- Plugins --
------------------------------------------------------------------------

-- fterm
map('n', '<C-t>', '<CMD>lua require("FTerm").toggle()<CR>', opts)
map('t', '<C-t>', '<C-\\><C-n><CMD>lua require("FTerm").toggle()<CR>', opts)

-- jabs
map('n', '<Leader>b', ':JABSOpen<CR>', opts)

-- nvim-transparent
map('n', '<Leader>0', ':TransparentToggle<CR>', opts)

-- nvim-tree
map('', '<C-n>', ':NvimTreeToggle<CR>', opts)

-- snippets
map('i', '<C-k>', '<CMD>lua return require"snippets".expand_or_advance(1)<CR>', opts)
map('i', '<C-j>', '<CMD>lua return require"snippets".advance_snippet(-1)<CR>', opts)

--  tagbar
map('n', '<F8>', ':TagbarToggle<CR>', opts)

-- telescope
map('n', '<Leader>ff', "<cmd>lua require('telescope.builtin').find_files()<CR>", {noremap = true})
map('n', '<Leader>fg', "<cmd>lua require('telescope.builtin').live_grep()<CR>", {noremap = true})
map('n', '<Leader>fb', "<cmd>lua require('telescope.builtin').buffers()<CR>", {noremap = true})
map('n', '<Leader>fh', "<cmd>lua require('telescope.builtin').help_tags()<CR>", {noremap = true})

-----------------------------------
-- plugin files with local keybinds
-- autopairs.lua
-- cmp.lua
-- gitsigns.lua
