vim.cmd [[
try
  colorscheme kanagawa
catch /^Vim\%((\a\+)\)\=:E185/
  colorscheme default
  set background=dark
endtry
]]

vim.o.syntax = 'enable'

vim.o.background = 'dark'
vim.o.t_Co = '256'
vim.o.hlsearch = true
vim.o.colorcolumn = '121'
vim.cmd('highlight ColorColumn ctermbg=235 guibg=grey15')
