local nvim_lsp = require'lspconfig'

local on_attach = function(client, bufnr)
    require'completion'.on_attach(client)
    buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
end
