return {
    settings = {
        Lua = {
            diagnostics = {
                globals = { "vim" } -- so that sumneko_lua recognizes the 'vim' variable
            },
            workspace = {
                library = {
                    [vim.fn.expand("$VIMRUNTIME/lua")] = true,
                    [vim.fn.stdpath("config") .. "/lua"] = true
                }
            }
        }
    }
}
